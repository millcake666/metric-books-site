from database.database import Base, get_session, engine
import os
import csv
from database.church import Church
from database.nasPunkt import NasPunkt
from database.uezd import Uezd
from database.data import Data
from sqlalchemy.sql import text

# Грузим таблицы в базу
Base.metadata.create_all(engine)
# Base.metadata.drop_all(engine)
# quit(0)

# Добавляем значения их csv файлов в базу
session = get_session()

csv_save_path = os.path.join(os.getcwd(), 'database', 'csv_export')

uezd_max_id = 0
np_max_id = 0
church_max_id = 0
data_max_id = 0

with open(os.path.join(csv_save_path, 'Uezd.csv'), 'r+', encoding='utf-8') as file:
    file_reader = csv.reader(file, delimiter=',', lineterminator='\r')
    rows = [row for row in file_reader]
    keys = rows[0]
    rows = rows[1:]

    for row in rows:
        u = Uezd(
            u_code=row[0],
            u_name=row[1]
        )
        session.add(u)
        if int(row[0]) > uezd_max_id:
            uezd_max_id = int(row[0])

    session.commit()

with open(os.path.join(csv_save_path, 'NasPunkt.csv'), 'r+', encoding='utf-8') as file:
    file_reader = csv.reader(file, delimiter=',', lineterminator='\r')
    rows = [row for row in file_reader]
    keys = rows[0]
    rows = rows[1:]

    for row in rows:
        u = NasPunkt(
            u_code=row[0],
            np_code=row[1],
            np_name=row[2]
        )
        session.add(u)
        if int(row[1]) > np_max_id:
            np_max_id = int(row[1])

    session.commit()

with open(os.path.join(csv_save_path, 'Church.csv'), 'r+', encoding='utf-8') as file:
    file_reader = csv.reader(file, delimiter=',', lineterminator='\r')
    rows = [row for row in file_reader]
    keys = rows[0]
    rows = rows[1:]

    for row in rows:
        u = Church(
            np_code=row[0],
            c_code=row[1],
            c_name=row[2]
        )
        session.add(u)
        if int(row[1]) > church_max_id:
            church_max_id = int(row[1])

    session.commit()

with open(os.path.join(csv_save_path, 'Data.csv'), 'r+', encoding='utf-8') as file:
    file_reader = csv.reader(file, delimiter=',', lineterminator='\r')
    rows = [row for row in file_reader]
    keys = rows[0]
    rows = rows[1:]

    for row in rows:
        u = Data(
            met_code=row[0],
            c_code=row[1],
            met_year=row[2],
            met_fond=row[3],
            met_opis=row[4],
            met_delo=row[5],
            met_page=row[6]
        )
        session.add(u)
        if int(row[0]) > data_max_id:
            data_max_id = int(row[0])

    session.commit()

session.execute(text(f'ALTER SEQUENCE "Uezd_u_code_seq" RESTART WITH {int(uezd_max_id) + 1}'))
session.execute(text(f'ALTER SEQUENCE "NasPunkt_np_code_seq" RESTART WITH {int(np_max_id) + 1}'))
session.execute(text(f'ALTER SEQUENCE "Church_c_code_seq" RESTART WITH {int(church_max_id) + 1}'))
session.execute(text(f'ALTER SEQUENCE "Data_met_code_seq" RESTART WITH {int(data_max_id) + 1}'))

session.commit()
