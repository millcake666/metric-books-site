from database.database import Base
from sqlalchemy import Column, Integer, Text, ForeignKey
from sqlalchemy.orm import relationship
from database.role import Role


class Users(Base):
    __tablename__ = 'Users'

    role_id = Column(Integer(), ForeignKey('Role.id'))
    id = Column(Integer(), primary_key=True, autoincrement=True)
    first_name = Column(Text(), nullable=False)  # имя
    middle_name = Column(Text(), nullable=False)  # отчество
    last_name = Column(Text(), nullable=False)  # фамилия
    login = Column(Text(), nullable=False)
    password = Column(Text(), nullable=False)

    role = relationship(Role)
