from typing import List

from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from database.database import get_session
from service.data import DataService
from scheme.data import Data

router = APIRouter(
    prefix='/data',
    tags=['Data']
)


@router.get('/{c_code}')
async def get(c_code: int, db: Session = Depends(get_session)) -> List[Data]:
    return DataService(db).get(c_code)


@router.delete('/{met_code}')
async def delete(met_code: int, db: Session = Depends(get_session)):
    return DataService(db).delete(met_code)


@router.patch('/{met_code}/{met_year}/{met_fond}/{met_opis}/{met_delo}/{met_page}')
async def patch(met_code: int, met_year: str, met_fond: str, met_opis: str, met_delo: str, met_page: str,
                db: Session = Depends(get_session)):
    return DataService(db).patch(met_code, met_year, met_fond, met_opis, met_delo, met_page)


@router.post('/{c_code}/{met_year}/{met_fond}/{met_opis}/{met_delo}/{met_page}')
async def add(c_code: int, met_year: str, met_fond: str, met_opis: str, met_delo: str, met_page: str,
              db: Session = Depends(get_session)):
    return DataService(db).add(c_code, met_year, met_fond, met_opis, met_delo, met_page)
