from typing import List

from pydantic import BaseModel
from scheme.church import ChurchDict


# class NasPunkt(BaseModel):
#     u_code: int
#     np_code: int
#     np_name: str

class Church(BaseModel):
    c_code: int
    c_name: str

    class Config:
        from_attributes = True


class NasPunkt(BaseModel):
    np_code: int
    np_name: str
    church: List[Church]

    class Config:
        from_attributes = True


class NasPunktDict(BaseModel):
    np_name: ChurchDict
